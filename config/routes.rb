Rails.application.routes.draw do

  resources :articles do
    resources :comments
  end

  resources :posts do
    resources :comments
  end

  resources :pages do
  end

  # get "/pages/:page" => "pages#show"

  root 'welcome#index'
end
